echo off
:menu
cls
echo Veuillez choisir votre action :
echo 1 - Generer un bundle
echo 2 - Mettre a jour la base de donnee
echo 3 - Creer la base de donnee
echo 4 - Nettoyer le cache
echo 5 - Installer les bundles via Composer
echo 6 - Generer une entity
set /p choix= Quel est votre choix :
if %choix% == 1 (php app/console generate:bundle)
if %choix% == 2 (php app/console doctrine:schema:update --force)
if %choix% == 3 (php app/console doctrine:database:create)
if %choix% == 4 (php app/console cache:clear)
if %choix% == 5 (php composer.phar update)
if %choix% == 6 (php app/console generate:doctrine:entity)

set /p loop = Appuyer sur une touche pour retourner au menu
goto menu
pause
