Installation de GSB sur un serveur linux:
- Mettre les fichiers contenus dans le dossier 'www' qui se trouve dans le dossier .zip sur le serveur web en utilisant
un client FTP (filezilla)
- Lancer le script installation.sh. Pendant cette phase, il vous sera demander:
 * l'adresse du serveur de la base de donnée ainsi que son port
 * le nom de la base de donnée
 * le login et le mot de passe d'un utilisateur pour se connecter à ce dernier
 Pour les autres champs, vous pouvez appuyer sur "entrée" pour laisser les valeurs par défaut.
- Lancer le script SQL 'gsb_frais_insert_tables_statiques.sql' sur cette base qui se trouve dans le dossier generation
- Ensuite se rendre dans le dossier "generation" via le navigateur et attendre la fin de la génération des données
- Vous pouvez maintenant effacer le dossier "generation" et le fichier installation.sh

NB : Pour pouvoir accèder au panel du développeur, il faudra rajouter votre adresse ip dans le fichier app_dev.php
Attention : il pourrait y avoir des erreurs au niveau des droits sur le dossier "app/cache"

L'installation est terminée.