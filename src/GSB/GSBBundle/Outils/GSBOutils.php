<?php

namespace GSB\GSBBundle\Outils;

class GSBOutils {

    /**
     * Récupère le mois suivant en fonction du mois actuel.
     *  
     * Renvoie le mois suivant ainsi que l'année.     * 
     * @param numéro mois
     * @return string au format 'YYYYmm'.
     */
    function getMoisSuivant($mois) {
        $numAnnee = substr($mois, 0, 4);
        $numMois = substr($mois, 4, 2);
        if ($numMois == "12") {
            $numMois = "01";
            $numAnnee++;
        } else {
            $numMois++;
        }
        if (strlen($numMois) == 1)
            $numMois = "0" . $numMois;
        return $numAnnee . $numMois;
    }
    
    /**
     * Récupère nom du mois en fonction de son numéro.
     * Ex : 12 = décembre 1 = janvier etc  
     * Renvoie le mois correspondant au numéro de celui-ci.      
     * @param int numéro du mois
     * @return string nom du mois.
     */
    public function obtenirLibelleMois($unNoMois) {
        $tabLibelles = array(1 => "Janvier",
            "Février", "Mars", "Avril", "Mai", "Juin", "Juillet",
            "Août", "Septembre", "Octobre", "Novembre", "Décembre");
        $libelle = "";
        if ($unNoMois >= 1 && $unNoMois <= 12) {
            $libelle = $tabLibelles[$unNoMois];
        }
        return $libelle;
    }
    
    /**
     * Compte le nombre de justificatifs d'une fiche hors forfait.
     *  
     * Renvoie le nombre de justificatifs donné.     
     * @param int ligne d'une fiche frais hors forfait
     * @return int nombre de justificatifs pour une fiche hors forfait.
     */
    public function compterNbJustificatif($idJeuEltsHorsForfait)
    {
        $nbJustificatif = 0;
        foreach($idJeuEltsHorsForfait as $uneLigneFHF){
            if($uneLigneFHF['justifie'] === true){
                $nbJustificatif++;
            }
        }
        return $nbJustificatif;
    }
   
    /**
     * Retourne un Tableau contenant l'année, le numéro du mois ainsi que son 
     * libelle pour chaque id(YYYYmm)
     *  
     * Renvoie le mois suivant ainsi que l'année.     * 
     * @param tableau contenant les numéros mois
     * @return array.
     */
    public function formaterTableauMois($tableauMois) {
        foreach ($tableauMois as $laLigne) {
            $mois = $laLigne['mois'];
            $numAnnee = substr($mois, 0, 4);
            $numMois = substr($mois, 4, 2);
            $lesMois["$mois"] = array(
                "mois" => "$mois",
                "numAnnee" => "$numAnnee",
                "numMois" => "$numMois",
                "libelleMois" => $this->obtenirLibelleMois(intVal($numMois))
            );
        }
        if (count($tableauMois) == 0) {
            return $tableauMois;
        } else {
            return $lesMois;
        }
    }

    /**
     * Vérifie si une chaîne fournie est bien numérique entière positive.                     
     * 
     * Retrourne true si la valeur transmise $valeur ne contient pas d'autres 
     * caractères que des chiffres, false sinon.
     * @param string chaîne à vérifier
     * @return boolean succès ou échec
     */
    function estEntierPositif($valeur) {
        return preg_match("/[^0-9]/", $valeur) == 0;
    }

    /**
     * Vérifie que chaque valeur est bien renseignée et numérique entière positive.
     *  
     * Renvoie la valeur booléenne true si toutes les valeurs sont bien renseignées et
     * numériques entières positives. False si l'une d'elles ne l'est pas.
     * @param array $lesValeurs tableau des valeurs
     * @return booléen succès ou échec
     */
    function verifierEntiersPositifs($lesValeurs) {
        $ok = true;
        foreach ($lesValeurs as $val) {
            if ($val == "" || !$this->estEntierPositif($val)) {
                $ok = false;
            }
        }
        return $ok;
    }

    /**
     * Indique si une date est incluse ou non dans l'année écoulée.
     * 
     * Retourne true si la date $date est comprise entre la date du jour moins un an et la 
     * la date du jour. False sinon.   
     * @param $date date au format jj/mm/aaaa
     * @return boolean succès ou échec
     */
    function estDansAnneeEcoulee($date) {
        $dateAnglais = $this->convertirDateFrancaisVersAnglais($date);
        $dateDuJourAnglais = date("Y-m-d");
        $dateDuJourMoinsUnAnAnglais = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d"), date("Y") - 1));
        return ($dateAnglais >= $dateDuJourMoinsUnAnAnglais) && ($dateAnglais <= $dateDuJourAnglais);
    }

    /**
     * Vérifie si une chaîne fournie est bien une date valide, au format JJ/MM/AAAA.                     
     * 
     * Retrourne true si la chaîne $date est une date valide, au format JJ/MM/AAAA, false sinon.
     * @param string date à vérifier
     * @return boolean succès ou échec
     */
    function estDate($date) {
        $tabDate = explode('/', $date);
        if (count($tabDate) != 3) {
            $dateOK = false;
        } elseif (!$this->verifierEntiersPositifs($tabDate)) {
            $dateOK = false;
        } elseif (!checkdate($tabDate[1], $tabDate[0], $tabDate[2])) {
            $dateOK = false;
        } else {
            $dateOK = true;
        }
        return $dateOK;
    }

    /**
     * Transforme une date au format français jj/mm/aaaa vers le format anglais aaaa-mm-jj
     * @param $date au format  jj/mm/aaaa
     * @return string la date au format anglais aaaa-mm-jj
     */
    function convertirDateFrancaisVersAnglais($date) {
        @list($jour, $mois, $annee) = explode('/', $date);
        return date("Y-m-d", mktime(0, 0, 0, $mois, $jour, $annee));
    }

    /**
     * Vérifie la validité des données d'une ligne de frais hors forfait.
     *  
     * Créer des messages flashs selon les erreurs rencontrées
     * Sur chaque donnée d'une ligne de frais hors forfait : vérifie que chaque 
     * donnée est bien renseignée, le montant est numérique positif, la date valide
     * et dans l'année écoulée.  
     * @param array $date date d'engagement de la ligne de frais HF
     * @param array $libelle libellé de la ligne de frais HF
     * @param array $montant montant de la ligne de frais HF
     * @param Session $session
     * @return boolean
     */
    function verifierLigneFraisHF($date, $libelle, $montant, $session) {
        // vérification du libellé
        $validation = true;
        if ($libelle == "") {
            $validation = false;
            $session->getFlashBag()->add('erreur', 'Le libellé doit être renseigné.');
        }
        // vérification du montant
        if ($montant == "") {
            $validation = false;
            $session->getFlashBag()->add('erreur', 'Le montant doit être renseigné.');
        } elseif (!is_numeric($montant) || $montant < 0) {
            $validation = false;
            $session->getFlashBag()->add('erreur', 'Le montant doit être numérique positif.');
        }
        // vérification de la date d'engagement
        if ($date == "") {
            $validation = false;
            $session->getFlashBag()->add('erreur', "La date d'engagement doit être renseignée.");
        } elseif (!$this->estDate($date)) {
            $validation = false;
            $session->getFlashBag()->add('erreur', "La date d'engagement doit être valide au format JJ/MM/AAAA");
        } elseif (!$this->estDansAnneeEcoulee($date)) {
            $validation = false;
            $session->getFlashBag()->add('erreur', "La date d'engagement doit se situer dans l'année écoulée");
        }
        return $validation;
    }

    /**
     * Permet de créer une nouvelle fiche de frais avec un idUser et un mois précis.
     * Exemple : Fiche du mois de décembre pour l'utilisateur a22.
     * 
     * @param EntityManager $em    
     * @param int $idUser
     * @param string au format 'YYYYmm' $mois     
     */
    public function creerFicheFrais($em, $idUser, $mois) {
        $repFichefrais = $em->getRepository('GSBGSBBundle:Fichefrais');
        $existeFicheFrais = $repFichefrais->getLesInfosFicheFrais($idUser, $mois);

        if (!$existeFicheFrais) {
            $repFichefrais->creeNouvellesLignesFrais($idUser, $mois);
        }
    }

    /**
     * Permet de formater un tableau pour avoir un autre tableau contenant:
     * - le libelle de ce Frais Forfait
     * - la quantité pour ce Frais Forfait
     * - le montant unitaire de ce Frais Forfait
     * - le total (montant unitaire * la quantité)
     * 
     * @param array $jeuEltsFraisForfait
     * @return array $jeuEltsFraisForfaitFormate   
     */
    public function formaterTableauEltsFraisForfait($jeuEltsFraisForfait) {
        $i = 0;
        foreach ($jeuEltsFraisForfait as $lgEltForfait) {
            $tab[$i]['libelle'] = $lgEltForfait->getIdFraisForfait()->getLibelle();
            $tab[$i]['quantite'] = $lgEltForfait->getQuantite();
            $tab[$i]['montantUnitaire'] = $lgEltForfait->getIdFraisForfait()->getMontant();
            $tab[$i]['total'] = intval($tab[$i]['quantite']) * floatval($tab[$i]['montantUnitaire']);
            $i++;
        }
        return $tab;
    }

    /**
     * Calcul total du montant des frais au forfait.
     * 
     * Renvoie le total du montant des frais au forfait.     
     * @param array frais au forfait pour une fiche frais
     * @return integer total des frais au forfait.
     */
    
    public function calculTotalEltsFraisForfait($tabEltsFraisForfait) {
        $total = 0;
        foreach ($tabEltsFraisForfait as $lgEltForfait) {
            $total += $lgEltForfait['total'];
        }
        return $total;
    }
    
    /**
     * Calcul du montant des frais hors forfait.
     * 
     * Renvoie le total du montant des frais hors forfait d'une fiche de frais.     
     * @param tableau frais hors forfait pour une fiche de frais.
     * @return int montant des frais hors forfait.
     */
    
    public function calculTotalEltsHorsForfait($tabEltsHorsForfait) {
        $total = 0;
        foreach ($tabEltsHorsForfait as $lgEltHorsForfait) {
            $total += $lgEltHorsForfait['montant'];
        }
        return $total;
    }
}
