<?php

namespace GSB\GSBBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class LoginController extends Controller {

    public function seconnecterAction(Request $requete) {
        if ($requete->getMethod() == 'POST') {
            $session = $requete->getSession();
            $login = $requete->get('txtLogin');
            $mdp = $requete->get('txtMdp');
            $em = $this->getDoctrine()->getManager();
            $repUtilisateur = $em->getRepository('GSBGSBBundle:Utilisateur');
            $utilisateur = $repUtilisateur->findOneBy(array('login' => $login));
            if ($utilisateur && password_verify($mdp, $utilisateur->getMdp())) {
                $session->set('user_id', $utilisateur->getId());
                $session->set('user_nom', $utilisateur->getNom());
                $session->set('user_prenom', $utilisateur->getPrenom());
                $session->set('user_role', $utilisateur->getRole()->getLibelle());
            } else {
                $session->getFlashBag()->add('erreur', 'Identifiant/Mot de passe incorrect');
            }
        }
        return $this->redirect($this->generateUrl('gsbgsb_homepage'));
    }

    public function sedeconnecterAction(Request $requete) {
        $session = $requete->getSession();
        $session->clear();
        return $this->redirect($this->generateUrl('gsbgsb_homepage'));
    }

}
