<?php

namespace GSB\GSBBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Contrôleur contenant toutes les actions ajax
 */
class RequeteAjaxController extends Controller {

    /**
     * Méthode permettant de reporter une ligne de frais hors forfait
     */
    public function reporterLFHFAction(Request $requete) {
        if ($requete->getMethod() == 'POST') {
            $idUtilisateur = $requete->get('idUtilisateur');
            $mois = $requete->get('idMois');
            $idFHF = $requete->get('idFHF');
            $session = $requete->getSession();
            $connecter = $session->has('user_id');
            if ($connecter) {
                $em = $this->getDoctrine()->getManager();
                $repUtilisateur = $em->getRepository('GSBGSBBundle:Utilisateur');
                if ($repUtilisateur->getRoleUtilisateur($session->get('user_id'))) {
                    $outils = $this->container->get('gsb_gsb.outils');
                    $repLFHF = $em->getRepository('GSBGSBBundle:Lignefraishorsforfait');
                    $repFichefrais = $em->getRepository('GSBGSBBundle:Fichefrais');

                    $moisSuivant = $outils->getMoisSuivant($mois);

                    $existeFicheFrais = $repFichefrais->getLesInfosFicheFrais($idUtilisateur, $moisSuivant);
                    if (!$existeFicheFrais) {
                        $repFichefrais->creeNouvellesLignesFrais($idUtilisateur, $moisSuivant);
                    }
                    $ficheFrais = $repFichefrais->findOneBy(array('idUtilisateur' => $idUtilisateur, 'mois' => $moisSuivant));

                    $LFHF = $repLFHF->find($idFHF);
                    $LFHF->setFicheFrais($ficheFrais);
                    $em->persist($LFHF);
                    $em->flush();

                    $response = new \Symfony\Component\HttpFoundation\Response(json_encode('Ligne Frais hors Forfait reportée avec succès'));
                    $response->setStatusCode(200);
                    $response->headers->set('Content-Type', 'application/json');
                    return $response;
                } else {
                    
                }
            }
        }
    }

    /**
     * Méthode permettant de supprimer une ligne frais hors forfait
     */
    public function supprimerLFHFAction(Request $requete) {
        if ($requete->getMethod() == 'POST') {
            $idFHF = $requete->get('idFHF');
            $session = $requete->getSession();
            $connecter = $session->has('user_id');
            if ($connecter) {
                $em = $this->getDoctrine()->getManager();
                $repUtilisateur = $em->getRepository('GSBGSBBundle:Utilisateur');
                if ($repUtilisateur->getRoleUtilisateur($session->get('user_id'))) {
                    $repLFHF = $em->getRepository('GSBGSBBundle:Lignefraishorsforfait');
                    $LFHF = $repLFHF->find($idFHF);
                    if (!preg_match('/^REFUSE : /', $LFHF->getLibelle())) {
                        $LFHF->setLibelle('REFUSE : ' . $LFHF->getLibelle());
                        $em->persist($LFHF);
                        $em->flush();
                    }
                    $response = new \Symfony\Component\HttpFoundation\Response(json_encode($LFHF->getLibelle()));
                    $response->setStatusCode(200);
                    $response->headers->set('Content-Type', 'application/json');
                    return $response;
                } else {
                    
                }
            }
        }
    }
    /**
     * Méthode permettant d'annuler le refus d'une ligne frais hors forfait
     */
    public function annulerRefusLFHFAction(Request $requete) {
        if ($requete->getMethod() == 'POST') {
            $idFHF = $requete->get('idFHF');
            $session = $requete->getSession();
            $connecter = $session->has('user_id');
            if ($connecter) {
                $em = $this->getDoctrine()->getManager();
                $repUtilisateur = $em->getRepository('GSBGSBBundle:Utilisateur');
                if ($repUtilisateur->getRoleUtilisateur($session->get('user_id'))) {
                    $repLFHF = $em->getRepository('GSBGSBBundle:Lignefraishorsforfait');
                    $LFHF = $repLFHF->find($idFHF);
                    $LFHF->setLibelle(preg_replace('/^REFUSE : /', '' ,$LFHF->getLibelle()));
                    $em->persist($LFHF);
                    $em->flush();
                    $response = new \Symfony\Component\HttpFoundation\Response(json_encode($LFHF->getLibelle()));
                    $response->setStatusCode(200);
                    $response->headers->set('Content-Type', 'application/json');
                    return $response;
                } else {
                    
                }
            }
        }
    }
    
    /**
     * Méthode permettant de changer la valeur d'un frais forfait d'une fiche de frais
     */
    public function changeValeurFFAction(Request $requete) {
        if ($requete->getMethod() == 'POST') {
            $idUtilisateur = $requete->get('idUtilisateur');
            $idMois = $requete->get('idMois');
            $nouvelleQuantite = $requete->get('nouvelleQuantite');
            $idFF = $requete->get('idFF');
            $session = $requete->getSession();
            $connecter = $session->has('user_id');
            $outils = $this->container->get('gsb_gsb.outils');
            if ($connecter && $session->get('user_role') == 'Comptable') {
                $em = $this->getDoctrine()->getManager();
                if ($outils->estEntierPositif($nouvelleQuantite)) {
                    $repFF = $em->getRepository('GSBGSBBundle:Fraisforfait');
                    $repLFF = $em->getRepository('GSBGSBBundle:Lignefraisforfait');
                    $LFF = $repLFF->findOneBy(array('idUtilisateur' => $idUtilisateur, 'mois' => $idMois, 'idFraisForfait' => $repFF->find($idFF)));
                    $LFF->setQuantite($nouvelleQuantite);
                    $em->persist($LFF);
                    $em->flush();
                    $response = new \Symfony\Component\HttpFoundation\Response(json_encode('Modification de la quantité du frais forfait effectuée'));
                    $response->setStatusCode(200);
                } else {
                    $response = new \Symfony\Component\HttpFoundation\Response(json_encode('La quantité n\'est pas un entier positif'));
                    $response->setStatusCode(409);
                }
                $response->headers->set('Content-Type', 'application/json');
                return $response;
            }
        }
    }

    /**
     * Méthode permettant de changer la valeur frais hors forfait d'une fiche de frais
     */
    public function changeValeurFHFAction(Request $requete) {
        if ($requete->getMethod() == 'POST') {
            $nouveauMontant = $requete->get('nouveauMontant');
            $idFHF = $requete->get('idFHF');
            $session = $requete->getSession();
            $connecter = $session->has('user_id');
            if ($connecter) {
                $em = $this->getDoctrine()->getManager();
                $repUtilisateur = $em->getRepository('GSBGSBBundle:Utilisateur');
                if ($repUtilisateur->getRoleUtilisateur($session->get('user_id')) == 'Comptable') {
                    if (is_numeric($nouveauMontant) && $nouveauMontant > 0) {
                        $repLFHF = $em->getRepository('GSBGSBBundle:Lignefraishorsforfait');
                        $LFHF = $repLFHF->find($idFHF);
                        $LFHF->setMontant($nouveauMontant);
                        $em->persist($LFHF);
                        $em->flush();
                        $response = new \Symfony\Component\HttpFoundation\Response(json_encode('Modification du montant du frais hors forfait effectuée'));
                        $response->setStatusCode(200);
                    } else {
                        $response = new \Symfony\Component\HttpFoundation\Response(json_encode('La montant n\'est pas un entier positif'));
                        $response->setStatusCode(409);
                    }
                    $response->headers->set('Content-Type', 'application/json');
                    return $response;
                } else {
                    
                }
            }
        }
    }

    /**
     * Méthode permettant de recupérer les mois qui ont une fiche de frais non valide pour un visiteur médical
     */
    public function recupMoisByVisiteurAction(Request $requete) {
        $idVisiteur = $requete->get('idVisiteur');
        $session = $requete->getSession();
        $connecter = $session->has('user_id');
        if ($connecter) {

            $em = $this->getDoctrine()->getManager();
            $repUtilisateur = $em->getRepository('GSBGSBBundle:Utilisateur');
            if ($repUtilisateur->getRoleUtilisateur($session->get('user_id')) == 'Comptable') {
                $lesUtilisateurs = $repUtilisateur->findBy(array('role' => 1));
                $outils = $this->container->get('gsb_gsb.outils');
                $repFichefrais = $em->getRepository('GSBGSBBundle:Fichefrais');
                $lesMois = $outils->formaterTableauMois($repFichefrais->getLesMoisDisponiblesNonValide($idVisiteur));
                if (count($lesMois) > 0) {
                    $response = new \Symfony\Component\HttpFoundation\Response(json_encode($lesMois));
                } else {
                    $response = new \Symfony\Component\HttpFoundation\Response(json_encode('Cette utilisateur n\'a pas de fiche de frais à valider'));
                }
                $response->headers->set('Content-Type', 'application/json');
                return $response;
            } else {
                
            }
        }
    }

    /**
     * Méthode permettant de recupérer les frais au forfait d'une fiche de frais
     */
    public function recupFraisAuForfaitAction(Request $requete) {
        $idUtilisateur = $requete->get('idUtilisateur');
        $idMois = $requete->get('idMois');
        $session = $requete->getSession();
        $connecter = $session->has('user_id');
        if ($connecter) {

            $em = $this->getDoctrine()->getManager();
            $repUtilisateur = $em->getRepository('GSBGSBBundle:Utilisateur');
            $repLFF = $em->getRepository('GSBGSBBundle:Lignefraisforfait');
            if ($repUtilisateur->getRoleUtilisateur($session->get('user_id')) == 'Comptable') {
                $LFF = $repLFF->getLesFraisForfait($idUtilisateur, $idMois);
                $response = new \Symfony\Component\HttpFoundation\Response(json_encode($LFF));
                $response->headers->set('Content-Type', 'application/json');
                return $response;
            } else {
                
            }
        }
    }

    /**
     * Méthode permettant de recupérer les frais hors forfait d'une fiche de frais
     */
    public function recupFraisHorsForfaitAction(Request $requete) {
        $idUtilisateur = $requete->get('idUtilisateur');
        $idMois = $requete->get('idMois');
        $session = $requete->getSession();
        $connecter = $session->has('user_id');
        if ($connecter) {

            $em = $this->getDoctrine()->getManager();
            $repUtilisateur = $em->getRepository('GSBGSBBundle:Utilisateur');
            $repLFHF = $em->getRepository('GSBGSBBundle:Lignefraishorsforfait');
            if ($repUtilisateur->getRoleUtilisateur($session->get('user_id')) == 'Comptable') {
                $LFHFs = $repLFHF->getLesFraisHorsForfait($idUtilisateur, $idMois);
                for ($i = 0; $i < count($LFHFs); $i++ ){
                    if (preg_match('/^REFUSE : /', $LFHFs[$i]['libelle'])) {
                        $LFHFs[$i]['refuser'] = true;
                    }else{
                        $LFHFs[$i]['refuser'] = false;
                    }
                }
                $response = new \Symfony\Component\HttpFoundation\Response(json_encode($LFHFs));
                $response->headers->set('Content-Type', 'application/json');
                return $response;
            } else {
                
            }
        }
    }

    /**
     * Méthode permettant de vérifier si une fiche de frais existe
     */
    public function verifExisteFicheFraisAction(Request $requete) {
        if ($requete->getMethod() == 'POST') {
            $idUtilisateur = $requete->get('idUtilisateur');
            $idMois = $requete->get('idMois');
            $session = $requete->getSession();
            $connecter = $session->has('user_id');
            if ($connecter) {

                $em = $this->getDoctrine()->getManager();
                $repUtilisateur = $em->getRepository('GSBGSBBundle:Utilisateur');
                $repFicheFrais = $em->getRepository('GSBGSBBundle:Fichefrais');
                if ($repUtilisateur->getRoleUtilisateur($session->get('user_id')) == 'Comptable') {
                    $ficheFrais = $repFicheFrais->getLesInfosFicheFrais($idUtilisateur, $idMois);
                    if (!$ficheFrais) {
                        $response = new \Symfony\Component\HttpFoundation\Response(json_encode('Pas de fiche frais pour ce visiteur ce mois'));
                        $response->setStatusCode(404);
                    } else {
                        $response = new \Symfony\Component\HttpFoundation\Response(json_encode('La fiche frais existe'));
                        $response->setStatusCode(200);
                    }
                } else {
                    $response = new \Symfony\Component\HttpFoundation\Response(json_encode('Vous n\'êtes pas autorisés à utiliser cette fonctionnalité'));
                    $response->setStatusCode(401);
                }
            } else {
                $response = new \Symfony\Component\HttpFoundation\Response(json_encode('Vous n\'êtes pas connectés'));
                $response->setStatusCode(401);
            }
        } else {
            $response = new \Symfony\Component\HttpFoundation\Response();
            $response->setStatusCode(400);
        }
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * Méthode permttant de mettre un justificatif sur une ligne de frais hors forfait d'une fiche de frais
     */
    public function modifJustifAction(Request $requete) {
        if ($requete->getMethod() == 'POST') {
            $idLFHF = $requete->get("idLFHF");
            $session = $requete->getSession();
            $connecter = $session->has('user_id');
            if ($connecter && $session->get('user_role') == 'Comptable') {
                $em = $this->getDoctrine()->getManager();
                $repLFHF = $em->getRepository('GSBGSBBundle:Lignefraishorsforfait');
                $LFHF = $repLFHF->find($idLFHF);
                if ($LFHF->getJustifie()) {
                    $LFHF->setJustifie(0);
                } else {
                    $LFHF->setJustifie(1);
                }
                $em->persist($LFHF);
                $em->flush();
                $response = new \Symfony\Component\HttpFoundation\Response(json_encode('Le justificatif a bien été pris en compte ! '));
                $response->setStatusCode(200);
                $response->headers->set('Content-Type', 'application/json');
                return $response;
            }
        }
    }

    /**
     * Méthode permettant de mettre en paiement une fiche de frais
     */
    public function miseEnPaiementFicheFraisAction(Request $requete) {
        if ($requete->getMethod() == 'POST') {
            $idUtilisateur = $requete->get('idUtilisateur');
            $mois = $requete->get('idMois');
            $session = $requete->getSession();
            $connecter = $session->has('user_id');
            if ($connecter) {
                $em = $this->getDoctrine()->getManager();
                $repUtilisateur = $em->getRepository('GSBGSBBundle:Utilisateur');
                if ($repUtilisateur->getRoleUtilisateur($session->get('user_id'))) {
                    $repLFHF = $em->getRepository('GSBGSBBundle:Lignefraishorsforfait');
                    $repLFF = $em->getRepository('GSBGSBBundle:Lignefraisforfait');
                    $repFichefrais = $em->getRepository('GSBGSBBundle:Fichefrais');
                    $repEtat = $em->getRepository('GSBGSBBundle:Etat');




                    $utilisateur = $repUtilisateur->find($idUtilisateur);
                    $etat = $repEtat->find('MP');
                    $etatVa = $repEtat->find('VA');
                    $ficheFrais = $repFichefrais->findOneBy(array('mois' => $mois, 'idUtilisateur' => $utilisateur, 'idetat' => $etatVa));
                    if ($ficheFrais) {
                        $ficheFrais->setIdEtat($etat);
                        $ficheFrais->setDateModif(new \DateTime());

                        $lesLFHF = $repLFHF->findBy(array('ficheFrais' => $ficheFrais));
                        $sommeMontantFraisForfait = $repLFF->getSommeFraisForfait($utilisateur, $mois);
                        $lesLFHF = $repLFHF->getLesFraisHorsForfait($utilisateur, $mois);

                        $em->persist($ficheFrais);
                        $em->flush();

                        $response = new \Symfony\Component\HttpFoundation\Response(json_encode('Fiche Frais Rem'));
                        $response->setStatusCode(200);
                        $response->headers->set('Content-Type', 'application/json');
                        $session->getFlashBag()->add('succes', 'Fiche mise en paiement');
                    } else {
                        $response = new \Symfony\Component\HttpFoundation\Response(json_encode('Fiche Frais Rem'));
                        $response->setStatusCode(404);
                        $session->getFlashBag()->add('erreur', 'La fiche frais ne peut pas étre mise en paiement');
                    }


                    //la banque rembourse
                    $etatRb = $repEtat->find('RB');
                    $ficheFrais->setIdEtat($etatRb);
                    $ficheFrais->setDateModif(new \DateTime());

                    $lesLFHF = $repLFHF->findBy(array('ficheFrais' => $ficheFrais));
                    $sommeMontantFraisForfait = $repLFF->getSommeFraisForfait($utilisateur, $mois);
                    $lesLFHF = $repLFHF->getLesFraisHorsForfait($utilisateur, $mois);

                    $em->persist($ficheFrais);
                    $em->flush();

                    return $response;
                } else {
                    $response = new \Symfony\Component\HttpFoundation\Response(json_encode('Fiche Frais Rem'));
                    $response->setStatusCode(404);
                    $session->getFlashBag()->add('erreur', "vous n'éte pas connecté");
                }
            }
        }
    }

}
