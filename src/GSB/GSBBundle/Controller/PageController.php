<?php

namespace GSB\GSBBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PageController extends Controller {

    private $_mois;

    public function __construct() {
        $this->_mois = sprintf("%04d%02d", date("Y"), date("m"));
    }

    public function accueilAction(Request $requete) {
        $session = $requete->getSession();
        $connecter = $session->has('user_id');
        if ($connecter) {
            $information = array('estConnecter' => $connecter);
        } else {
            $information = array('estConnecter' => $connecter);
        }
        return $this->render('GSBGSBBundle:Page:index.html.twig', $information);
    }

    public function saisieFicheFraisAction(Request $requete) {
        $session = $requete->getSession();
        $connecter = $session->has('user_id');
        if ($connecter && $session->get('user_role') == 'Visiteur Médical') {
            $outils = $this->container->get('gsb_gsb.outils');
            $em = $this->getDoctrine()->getManager();
            $idUser = $session->get('user_id');
            $repLigneFraisForfait = $em->getRepository('GSBGSBBundle:Lignefraisforfait');
            $repLigneFraisHorsForfait = $em->getRepository('GSBGSBBundle:Lignefraishorsforfait');

            $outils->creerFicheFrais($em, $idUser, $this->_mois);

            $ligneFraisForfait = $repLigneFraisForfait->getLesFraisForfait($idUser, $this->_mois);
            $ligneFraisHorsForfait = $repLigneFraisHorsForfait->getLesFraisHorsForfait($idUser, $this->_mois);

            $information = array('estConnecter' => $connecter, 'libelleMois' => $outils->obtenirLibelleMois(intval(substr($this->_mois, 4, 2))), 'annee' => substr($this->_mois, 0, 4),
                'ligneFraisForfait' => $ligneFraisForfait,
                'ligneFraisHorsForfait' => $ligneFraisHorsForfait);
            return $this->render('GSBGSBBundle:Page:saisieFicheFrais.html.twig', $information);
        } else {
            return $this->redirect($this->generateUrl('gsbgsb_homepage'));
        }
    }

    public function consultFichesFraisAction(Request $requete) {
        $session = $requete->getSession();
        $connecter = $session->has('user_id');
        if ($connecter && $session->get('user_role') == 'Visiteur Médical') {
            $idUser = $session->get('user_id');
            $outils = $this->container->get('gsb_gsb.outils');
            $em = $this->getDoctrine()->getManager();
            $repFichefrais = $em->getRepository('GSBGSBBundle:Fichefrais');

            $lesMois = $outils->formaterTableauMois($repFichefrais->getLesMoisDisponibles($idUser));

            if ($requete->getMethod() == 'POST') {
                $moisSaisi = $requete->get('lstMois');
                $existeFicheFrais = $repFichefrais->getLesInfosFicheFrais($idUser, $moisSaisi);

                if (!$existeFicheFrais) {
                    $session->getFlashBag()->add('erreur', "Le mois demandé est invalide");
                    return $this->redirect($this->generateUrl('gsbgsb_consultFichesFrais'));
                } else {
                    $repLigneFraisForfait = $em->getRepository('GSBGSBBundle:Lignefraisforfait');
                    $repLigneFraisHorsForfait = $em->getRepository('GSBGSBBundle:Lignefraishorsforfait');

                    $jeuEltsFraisForfait = $repLigneFraisForfait->findBy(array('idUtilisateur' => $idUser, 'mois' => $moisSaisi));

                    $tab = $outils->formaterTableauEltsFraisForfait($jeuEltsFraisForfait);

                    $idJeuEltsHorsForfait = $repLigneFraisHorsForfait->getLesFraisHorsForfait($idUser, $moisSaisi);
                    $nbJustificatif = $outils->compterNbJustificatif($idJeuEltsHorsForfait);

                    $information = array('estConnecter' => $connecter, 'mois' => $lesMois, 'consultActif' => true, 'libelleMois' => $outils->obtenirLibelleMois(intval(substr($requete->get('lstMois'), 4, 2))), 'idEtat' => $existeFicheFrais['idEtat'],
                        'annee' => substr($moisSaisi, 0, 4), 'moisSaisi' => $moisSaisi, 'libelleEtat' => $existeFicheFrais['libEtat'], 'dateModif' => $existeFicheFrais['dateModif']->format('Y-m-d'),
                        'montantValide' => $existeFicheFrais['montantValide'], 'tabEltsFraisForfait' => $tab, 'idJeuEltsHorsForfait' => $idJeuEltsHorsForfait, 'nbJustificatif' => $nbJustificatif);
                }
            } else {
                $information = array('estConnecter' => $connecter, 'mois' => $lesMois, 'consultActif' => false, 'moisSaisi' => null);
            }
            return $this->render('GSBGSBBundle:Page:consultFichesFrais.html.twig', $information);
        } else {
            return $this->redirect($this->generateUrl('gsbgsb_homepage'));
        }
    }

    public function gestionFicheFraisAction(Request $requete) {
        $session = $requete->getSession();
        $connecter = $session->has('user_id');
        if ($connecter) {

            $em = $this->getDoctrine()->getManager();
            $repUtilisateur = $em->getRepository('GSBGSBBundle:Utilisateur');
            $repFraisForfait = $em->getRepository('GSBGSBBundle:Fraisforfait');
            $utilisateur = $repUtilisateur->getInfosUtilisateur($session->get('user_id'));
            if ($repUtilisateur->getRoleUtilisateur($session->get('user_id')) == 'Comptable') {
                $lesUtilisateurs = $repUtilisateur->findBy(array('role' => 1));
                $tab = $repFraisForfait->getLesFrais();
                $information = array('estConnecter' => $connecter, 'utilisateur' => $utilisateur, 'lesUtilisateurs' => $lesUtilisateurs, 'tabEltsFraisForfait' => $tab);
                return $this->render('GSBGSBBundle:Page:gestionFicheFrais.html.twig', $information);
            } else {
                return $this->redirect($this->generateUrl('gsbgsb_homepage'));
            }
        }
        return $this->redirect($this->generateUrl('gsbgsb_homepage'));
    }

    public function genererPdfFicheFraisAction(Request $requete) {
        $session = $requete->getSession();
        $connecter = $session->has('user_id');
        if ($connecter && $session->get('user_role') == 'Visiteur Médical') {
            if ($requete->getMethod() == 'GET') {
                $outils = $this->container->get('gsb_gsb.outils');
                $moisSaisi = $requete->get('mois');
                $em = $this->getDoctrine()->getManager();
                $repFichefrais = $em->getRepository('GSBGSBBundle:Fichefrais');
                $repLigneFraisForfait = $em->getRepository('GSBGSBBundle:Lignefraisforfait');
                $repLigneFraisHorsForfait = $em->getRepository('GSBGSBBundle:Lignefraishorsforfait');


                $existeFicheFrais = $repFichefrais->getLesInfosFicheFrais($session->get('user_id'), $moisSaisi);
                if ($existeFicheFrais) {


                    if ($existeFicheFrais['idEtat'] === 'MP' || $existeFicheFrais['idEtat'] === 'RB') {

                        $cheminRep = $this->get('kernel')->getRootDir() . '\\..\\web\\gsbbundle\\pdf\\';

                        if (!file_exists($cheminRep)) {
                            mkdir($cheminRep, 0700);
                        }

                        $nomPdf = 'ficheFrais_' . $session->get('user_id') . '_' . strtoupper($session->get('user_nom')) . $session->get('user_prenom') . '_' . $moisSaisi . '.pdf';


                        if (!file_exists($cheminRep . $nomPdf)) {
                            $idJeuEltsHorsForfait = $repLigneFraisHorsForfait->getLesFraisHorsForfait($session->get('user_id'), $moisSaisi);
                            $jeuEltsFraisForfait = $repLigneFraisForfait->findBy(array('idUtilisateur' => $session->get('user_id'), 'mois' => $moisSaisi));



                            $tab = $outils->formaterTableauEltsFraisForfait($jeuEltsFraisForfait);
                            $totalEltsFraisForfait = $outils->calculTotalEltsFraisForfait($tab);
                            $totalEltsHorsForfait = $outils->calculTotalEltsHorsForfait($idJeuEltsHorsForfait);

                            $pdf = $this->container->get('white_october.tcpdf')->create();
                            $pdf->SetTitle('Remboursement de frais engages');
                            $pdf->SetAuthor('Galaxy Swiss Bourdin');
                            $pdf->setPrintHeader(false);
                            $pdf->setPrintFooter(false);

                            $pdf->setMargins(PDF_MARGIN_LEFT, 20, PDF_MARGIN_RIGHT);
                            $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
                            $pdf->AddPage();
                            $html = $this->renderView('GSBGSBBundle:Pdf:pdfFicheFrais.html.twig', array(
                                'nomUtilisateur' => $session->get('user_nom'),
                                'prenomUtilisateur' => $session->get('user_prenom'),
                                'idUtilisateur' => $session->get('user_id'),
                                'libelleEtat' => $existeFicheFrais['libEtat'],
                                'montantValide' => $existeFicheFrais['montantValide'],
                                'tabEltsFraisForfait' => $tab,
                                'totalEltsFraisForfait' => $totalEltsFraisForfait,
                                'idJeuEltsHorsForfait' => $idJeuEltsHorsForfait,
                                'totalEltsHorsForfait' => $totalEltsHorsForfait,
                                'libelleMois' => $outils->obtenirLibelleMois(intval(substr($moisSaisi, 4, 2))),
                                'dateSignature' => date("d") . ' ' . $outils->obtenirLibelleMois(intval(substr($this->_mois, 4, 2))) . ' ' . substr($this->_mois, 0, 4),
                                'annee' => substr($moisSaisi, 0, 4)
                            ));

                            $pdf->writeHTML($html, true, false, false, false, '');
                            $pdf->Output($cheminRep . $nomPdf, 'F');
                        }

                        return new Response(file_get_contents($cheminRep . $nomPdf), 200, array(
                            'Content-Type' => 'application/pdf',
                            'Content-Disposition' => 'attachment; filename="' . $nomPdf . '"'
                        ));
                    } else {
                        $session->getFlashBag()->add('erreur', "Cette fiche frais n'est pas mise en paiement ou remboursée");
                        return $this->redirect($this->generateUrl('gsbgsb_consultFichesFrais'));
                    }
                } else {
                    $session->getFlashBag()->add('erreur', "Le mois demandé est invalide");
                    return $this->redirect($this->generateUrl('gsbgsb_consultFichesFrais'));
                }
            }
        } else {
            return $this->redirect($this->generateUrl('gsbgsb_homepage'));
        }
    }

    public function suivreFicheFraisAction(Request $requete) {
        $session = $requete->getSession();
        $connecter = $session->has('user_id');
        if ($connecter) {

            $em = $this->getDoctrine()->getManager();


            $repUtilisateur = $em->getRepository('GSBGSBBundle:Utilisateur');
            $utilisateur = $repUtilisateur->getInfosUtilisateur($session->get('user_id'));
            if ($repUtilisateur->getRoleUtilisateur($session->get('user_id')) == 'Comptable') {

                $repFicheFrais = $em->getRepository('GSBGSBBundle:Fichefrais');
                $ficheFraisValide = $repFicheFrais->getFicheFraisValide();




                $outils = $this->container->get('gsb_gsb.outils');

                for ($i = 0; $i < count($ficheFraisValide); $i++) {
                    $ficheFraisValide[$i]['libelleDate'] = $outils->obtenirLibelleMois(intval(substr($ficheFraisValide[$i]['fiche_mois'], 4, 2))) . ' ' . substr($ficheFraisValide[$i]['fiche_mois'], 0, 4);
                }

                $information = array('estConnecter' => $connecter, 'utilisateur' => $utilisateur, 'ligneFicheValider' => $ficheFraisValide);
                return $this->render('GSBGSBBundle:Page:suivreFicheFrais.html.twig', $information);
            } else {
                return $this->redirect($this->generateUrl('gsbgsb_homepage'));
            }
        }
    }

}
