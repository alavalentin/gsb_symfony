<?php

namespace GSB\GSBBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class OperationBddController extends Controller {

    private $_mois;

    public function __construct() {
        $this->_mois = sprintf("%04d%02d", date("Y"), date("m"));
    }

    /**
     * Méthode permettant d'ajout un frais hors forfait dont les informations sont passées via la méthode POST
     */
    public function validAjoutElementHsForfaitAction(Request $requete) {
        $session = $requete->getSession();
        if ($session->has('user_id')) {
            $outils = $this->container->get('gsb_gsb.outils');
            $dateHF = $requete->get('txtDateHF');
            $libelleHF = $requete->get('txtLibelleHF');
            $montantHF = $requete->get('txtMontantHF');

            if ($outils->verifierLigneFraisHF($dateHF, $libelleHF, $montantHF, $session)) {
                $em = $this->getDoctrine()->getManager();
                $repLigneFraisHorsForfait = $em->getRepository('GSBGSBBundle:Lignefraishorsforfait');

                // la nouvelle ligne ligne doit être ajoutée dans la base de données
                $repLigneFraisHorsForfait->creeNouveauFraisHorsForfait($session->get('user_id'), $this->_mois, $libelleHF, $outils->convertirDateFrancaisVersAnglais($dateHF), $montantHF, false);
                $session->getFlashBag()->add('succes', 'Les modifications de la fiche de frais ont bien été enregistrées');
            } else {
                $session->getFlashBag()->add('libelle', $libelleHF);
                $session->getFlashBag()->add('montant', $montantHF);
                $session->getFlashBag()->add('date', $dateHF);
            }
            return $this->redirect($this->generateUrl('gsbgsb_saisieFicheFrais'));
        } else {
            return $this->redirect($this->generateUrl('gsbgsb_homepage'));
        }
    }

    /**
     * Supprime le frais hors forfait dont l'id est passé via la méthode POST
     */
    public function supprimerFraisHorsForfaitAction(Request $requete) {
        $session = $requete->getSession();
        $connecter = $session->has('user_id');
        if ($connecter) {
            $em = $this->getDoctrine()->getManager();
            $repLigneFraisHorsForfait = $em->getRepository('GSBGSBBundle:Lignefraishorsforfait');
            $uneLigneFraisHsforfait = $repLigneFraisHorsForfait->find($requete->get('idLigneHF'));
            
            // Vérifie si la ligne frais hors forfait est bien à l'utilisateur et qu'elle appartient à une fiche 
            // où la saisie est en cours
            if ($uneLigneFraisHsforfait->getFicheFrais()->getIdUtilisateur()->getId() === $session->get('user_id')
                    && $uneLigneFraisHsforfait->getFicheFrais()->getIdEtat()->getId() === 'CR') {
                $em->remove($uneLigneFraisHsforfait);
                $em->flush();
            } else {
                $session->getFlashBag()->add('erreur','Suppression de cette élément hors forfait impossible');
            }
            return $this->redirect($this->generateUrl('gsbgsb_saisieFicheFrais'));
        } else {
            return $this->redirect($this->generateUrl('gsbgsb_homepage'));
        }
    }

    /**
     * Méthode permettant de modifier les frais au forfait avec la quantité passée via la méthode POST
     */
    public function validModifdElementForfaitAction(Request $requete) {
        $session = $requete->getSession();
        $connecter = $session->has('user_id');
        if ($connecter) {
            $outils = $this->container->get('gsb_gsb.outils');
            $tabQteEltsForfait = $requete->get('txtEltsForfait');
            $em = $this->getDoctrine()->getManager();
            $repLigneFraisForfait = $em->getRepository('GSBGSBBundle:Lignefraisforfait');
            $ok = $outils->verifierEntiersPositifs($tabQteEltsForfait);
            if (!$ok) {
                $session->getFlashBag()->add('erreur', 'Chaque quantité doit être renseignée et numérique positive.');
            } else { // mise à jour des quantités des éléments forfaitisés
                $repLigneFraisForfait->majFraisForfait($session->get('user_id'), $this->_mois, $tabQteEltsForfait);
            }
            return $this->redirect($this->generateUrl('gsbgsb_saisieFicheFrais'));
        } else {
            return $this->redirect($this->generateUrl('gsbgsb_homepage'));
        }
    }

    /**
     * Méthode permettant de valider une fiche de frais
     */
    public function validFicheFraisAction(Request $requete) {
        if ($requete->getMethod() == 'POST') {
            $idUtilisateur = $requete->get('lstVisiteur');
            $mois = $requete->get('lstMois');
            $session = $requete->getSession();
            $connecter = $session->has('user_id');
            if ($connecter && $session->get('user_role') == 'Comptable') {
                $em = $this->getDoctrine()->getManager();
                $repUtilisateur = $em->getRepository('GSBGSBBundle:Utilisateur');
                $repLFHF = $em->getRepository('GSBGSBBundle:Lignefraishorsforfait');
                $repLFF = $em->getRepository('GSBGSBBundle:Lignefraisforfait');
                $repFichefrais = $em->getRepository('GSBGSBBundle:Fichefrais');
                $repEtat = $em->getRepository('GSBGSBBundle:Etat');


                $utilisateur = $repUtilisateur->find($idUtilisateur);
                $etat = $repEtat->find('VA');

                $ficheFrais = $repFichefrais->findOneBy(array('mois' => $mois, 'idUtilisateur' => $utilisateur));

                $ficheFrais->setIdEtat($etat);
                $ficheFrais->setDateModif(new \DateTime());

                $lesLFHF = $repLFHF->findBy(array('ficheFrais' => $ficheFrais));
                $sommeMontantFraisForfait = $repLFF->getSommeFraisForfait($utilisateur, $mois);
                $lesLFHF = $repLFHF->getLesFraisHorsForfait($utilisateur, $mois);

                $sommeMontantFraisHorsForfait = 0;

                foreach ($lesLFHF as $uneLFHF) {
                    if (!preg_match('/^REFUSE : /', $uneLFHF['libelle'])) {
                        $sommeMontantFraisHorsForfait += intval($uneLFHF['montant']);
                    }
                }

                $montantValide = $sommeMontantFraisForfait + $sommeMontantFraisHorsForfait;

                $ficheFrais->setMontantValide($montantValide);
                $em->persist($ficheFrais);
                $em->flush();
                $session->getFlashBag()->add('succes', 'La fiche de frais a bien été validée');
                return $this->redirect($this->generateUrl('gsbgsb_gestionFicheFrais'));
            } else {
                return $this->redirect($this->generateUrl('gsbgsb_homepage'));
            }
        }
    }

}
