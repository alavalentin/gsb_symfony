<?php

namespace GSB\GSBBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * lignefraisforfait
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="GSB\GSBBundle\Entity\LignefraisforfaitRepository")
 */
class Lignefraisforfait
{

    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(name="idUtilisateur", type="string", length=4)
     */
    private $idUtilisateur;

    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(name="mois", type="string", length=6)
     */
    private $mois;

    /**
     * @var \Fraisforfait
     * @ORM\ManyToOne(targetEntity="Fraisforfait")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idFraisforfait", referencedColumnName="id")
     * })
     * 
     * @ORM\Id
     */
    private $idFraisForfait;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantite", type="integer", nullable=true)
     */
    private $quantite;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idVisiteur
     *
     * @param string $idVisiteur
     *
     * @return lignefraisforfait
     */
    public function setIdUtilisateur($idUtilisateur)
    {
        $this->idUtilisateur = $idUtilisateur;

        return $this;
    }

    /**
     * Get idVisiteur
     *
     * @return string
     */
    public function getIdUtilisateur()
    {
        return $this->idUtilisateur;
    }

    /**
     * Set mois
     *
     * @param string $mois
     *
     * @return lignefraisforfait
     */
    public function setMois($mois)
    {
        $this->mois = $mois;

        return $this;
    }

    /**
     * Get mois
     *
     * @return string
     */
    public function getMois()
    {
        return $this->mois;
    }

    /**
     * Set idFraisForfait
     *
     * @param string $idFraisForfait
     *
     * @return lignefraisforfait
     */
    public function setIdFraisForfait($idFraisForfait)
    {
        $this->idFraisForfait = $idFraisForfait;

        return $this;
    }

    /**
     * Get idFraisForfait
     *
     * @return string
     */
    public function getIdFraisForfait()
    {
        return $this->idFraisForfait;
    }

    /**
     * Set quantite
     *
     * @param integer $quantite
     *
     * @return lignefraisforfait
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;

        return $this;
    }

    /**
     * Get quantite
     *
     * @return integer
     */
    public function getQuantite()
    {
        return $this->quantite;
    }
    
}

