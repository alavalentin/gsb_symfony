<?php

namespace GSB\GSBBundle\Entity;

/**
 * lignefraisforfaitRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class LignefraishorsforfaitRepository extends \Doctrine\ORM\EntityRepository {

    /**
     * Retourne sous forme d'un tableau associatif toutes les lignes de frais hors forfait
     * concernées par les deux arguments

     * @param $idVisiteur 
     * @param $mois sous la forme aaaamm
     * @return tous les champs des lignes de frais hors forfait sous la forme d'un tableau associatif 
     */
    public function getLesFraisHorsForfait($idVisiteur, $mois) {
        $qb = $this->createQueryBuilder('lfhf');
        $qb->select('lfhf')
                ->innerJoin('lfhf.ficheFrais', 'fiche')
                ->where('fiche.idUtilisateur = :id')
                ->setParameter('id', $idVisiteur)
                ->andWhere('fiche.mois = :mois')
                ->setParameter('mois', $mois);

        return $qb->getQuery()->getArrayResult();
    }

    /**
     * Crée un nouveau frais hors forfait pour un visiteur à un mois donné
     * à partir des informations fournies en paramètre

     * @param $idVisiteur 
     * @param $mois sous la forme aaaamm
     * @param $libelle : le libelle du frais
     * @param $date : la date du frais au format français jj/mm/aaaa
     * @param $montant : le montant
     * @param $justifie : Vrai si le frais hors forfait à son justificatif
     */
    public function creeNouveauFraisHorsForfait($idVisiteur, $mois, $libelle, $date, $montant,$justifie) {
        $em = $this->getEntityManager();
        $ficheFrais = $em->getRepository('GSBGSBBundle:Fichefrais')->findBy(array('idUtilisateur' => $idVisiteur,'mois' => $mois));
        
        $uneLigneHsForfait = new Lignefraishorsforfait();
        $uneLigneHsForfait->setFicheFrais($ficheFrais[0]);
        $uneLigneHsForfait->setDate(new \DateTime($date));
        $uneLigneHsForfait->setMontant($montant);
        $uneLigneHsForfait->setLibelle($libelle);
        $uneLigneHsForfait->setJustifie($justifie);
        $em->persist($uneLigneHsForfait);
        $em->flush();
        
        
    }
    
    /**
     * Récupère la somme des frais hors forfait.
     * @param $utilisateur 
     * @param $mois sous la forme aaaamm   
     * @return la somme des frais hors forfait.  
     */
    public function getSommeFraisHorsForfait($utilisateur,$mois){
        $qb = $this->createQueryBuilder('lfhf');
        $qb->select('SUM(lfhf.montant)')
                ->innerJoin('lfhf.ficheFrais', 'fiche')
                ->where('fiche.idUtilisateur = :id')
                ->setParameter('id', $utilisateur)
                ->andWhere('fiche.mois = :mois')
                ->setParameter('mois', $mois);

        return $qb->getQuery()->getSingleScalarResult();
    }
}
