<?php

namespace GSB\GSBBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Lignefraishorsforfait
 *
 * @ORM\Table(name="lignefraishorsforfait", indexes={@ORM\Index(name="idUtilisateur", columns={"idUtilisateur", "mois"})})
 * @ORM\Entity(repositoryClass="GSB\GSBBundle\Entity\LignefraishorsforfaitRepository")
 */
class Lignefraishorsforfait
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=100, nullable=true)
     */
    private $libelle;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=true)
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="montant", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $montant;

    /**
     * @var \Fichefrais
     *
     * @ORM\ManyToOne(targetEntity="Fichefrais")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idUtilisateur", referencedColumnName="idUtilisateur", nullable=false,onDelete="CASCADE"),
     *   @ORM\JoinColumn(name="mois", referencedColumnName="mois", nullable=false,onDelete="CASCADE")
     * })
     */
    private $ficheFrais;
    
     /**
     * @var boolean
     *
     * @ORM\Column(name="justifie", type="boolean", nullable=false, options={"default":0})
     */
    private $justifie;
    
    public function getDate()
    {
        return $this->date;
    }
    
    public function getLibelle()
    {
        return $this->libelle;
    }
    
    public function getMontant()
    {
        return $this->montant;
    }
    
    public function getId()
    {
       return $this->id;
    }
    
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }
    public function setFicheFrais($idFicheFrais)
    {
        $this->ficheFrais = $idFicheFrais;

        return $this;
    }
    
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }
    
    public function setMontant($montant)
    {
        $this->montant = $montant;

        return $this;
    }
    
    public function setJustifie($justifie){
        $this->justifie = $justifie;

        return $this;
    }
    
    public function getFicheFrais()
    {
        return $this->ficheFrais;
    }
    
    public function getJustifie() {
        return $this->justifie;
    }
}

