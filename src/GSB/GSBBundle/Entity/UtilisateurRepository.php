<?php

namespace GSB\GSBBundle\Entity;

class UtilisateurRepository extends \Doctrine\ORM\EntityRepository {

    /**
     * Retourne les informations d'un visiteur

     * @param $login 
     * @param $mdp
     * @return l'id, le nom et le prénom sous la forme d'un tableau associatif 
     */
    public function estUnUtilisateur($login, $mdp) {
        $qb = $this->createQueryBuilder('v');
        $qb->select('v.id')
                ->where('v.login = :login')
                ->setParameter('login', $login)
                ->andWhere('v.mdp = :mdp')
                ->setParameter('mdp', $mdp);
        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * Retourne les informations d'un visiteur

     * @param $id
     * @return le login, le nom et le prénom sous la forme d'un tableau associatif 
     */
    public function getInfosUtilisateur($id) {
        $qb = $this->createQueryBuilder('v');
        $qb->select('v.nom, v.prenom, r.libelle')
                ->innerJoin('v.role', 'r')
                ->where('v.id = :id')
                ->setParameter('id', $id);
        
        return $qb->getQuery()->getSingleResult();
    }
    
    
    /**
     * Retourne le rôle d'un utilisateur.

     * @param $id
     * @return le rôle de l'utilisateur.
     */
    public function getRoleUtilisateur($id) {
        $qb = $this->createQueryBuilder('v');
        $qb->select('r.libelle')
                ->innerJoin('v.role', 'r')
                ->where('v.id = :id')
                ->setParameter('id', $id);
        
        return $qb->getQuery()->getSingleScalarResult();
    }
   

}
