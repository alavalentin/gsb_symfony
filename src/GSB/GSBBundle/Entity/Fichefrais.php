<?php

namespace GSB\GSBBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Fichefrais
 *
 * @ORM\Table(name="fichefrais", indexes={@ORM\Index(name="idEtat", columns={"idEtat"}), @ORM\Index(name="IDX_92D5AB081D06ADE3", columns={"idUtilisateur"})})
 * @ORM\Entity(repositoryClass="GSB\GSBBundle\Entity\FichefraisRepository")
 */
class Fichefrais
{
    /**
     * @var string
     *
     * @ORM\Column(name="mois", type="string", length=6, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $mois;

    /**
     * @var string
     *
     * @ORM\Column(name="montantValide", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $montantvalide;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateModif", type="date", nullable=true)
     */
    private $datemodif;

    /**
     * @var \Etat
     *
     * @ORM\ManyToOne(targetEntity="Etat")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idEtat", referencedColumnName="id")
     * })
     */
    private $idetat;

    /**
     * @var \Visiteur
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idUtilisateur", referencedColumnName="id")
     * })
     */
    private $idUtilisateur;


    public function setIdEtat($idEtat){
        $this->idetat = $idEtat;
        return $this;
    }
    
    public function setIdUtilisateur($idVisiteur){
        $this->idUtilisateur = $idVisiteur;
        return $this;
    }
    
    public function setMois($mois){
        $this->mois = $mois;
        return $this;
    }
    
    public function setMontantValide($montantValide){
        $this->montantvalide = $montantValide;
        return $this;
    }
    
    public function setDateModif($dateModif){
        $this->datemodif = $dateModif;
        return $this;
    }
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idfraisforfait = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    public function getIdUtilisateur()
    {
        return $this->idUtilisateur;
    }

    public function getIdEtat()
    {
        return $this->idetat;
    }
}

